package it.mobile.android.pandorum.feature.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.View.GONE
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.feature.home.fragment.BookingFragment
import it.mobile.android.pandorum.feature.home.fragment.GameDetailFragment
import it.mobile.android.pandorum.feature.home.fragment.GameTabFragment
import it.mobile.android.pandorum.feature.home.fragment.HomeTabFragment
import it.mobile.android.pandorum.feature.login.LoginActivity
import it.mobile.android.pandorum.model.Game
import it.mobile.android.pandorum.widget.BookingButton
import it.mobile.android.pandorum.widget.GamesButton
import it.mobile.android.pandorum.widget.HomeButton
import it.mobile.android.pandorum.widget.Toolbar
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar

open class HomeActivity : AppCompatActivity() {

    private lateinit var mHomeButton: HomeButton
    private lateinit var mGameButton: GamesButton
    private lateinit var mBookingButton :BookingButton
    private lateinit var mHomeButtons: View
    private lateinit var mToolbar: Toolbar
    private lateinit var mUnregistrar: Unregistrar

    companion object {
        const val HOME_BUTTON_ID: String = "ID_1"
        const val GAMES_BUTTON_ID: String = "ID_2"
        const val CALENDAR_BUTTON_ID: String = "ID_3"
        const val HOME_TAG: String = "HOME_TAG_FRAGMENT"
        const val GAME_TAG: String = "GAME_TAG_FRAGMENT"
        const val BOOKING_TAG: String = "BOOKING_TAG"
        const val GAME_DETAIL_TAG: String = "GAME_DETAIL_TAG_FRAGMENT"
    }

    private val homeButtonSubject: Subject<String> = PublishSubject.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mToolbar = findViewById(R.id.toolbar)
        mHomeButtons = findViewById(R.id.button)
        mHomeButton = findViewById(R.id.home_button)
        mGameButton = findViewById(R.id.homeGameButton)
        mBookingButton = findViewById(R.id.homecalendarButton)
        mHomeButton.isSelected = true
        mHomeButton.setPublishSubject(homeButtonSubject)
        mGameButton.setPublishSubject(homeButtonSubject)
        mBookingButton.setPublishSubject(homeButtonSubject)
        mHomeButton.setOnClickListener {
            homeButtonSubject.onNext(HOME_BUTTON_ID)
            switchToHomeFragment()
        }
        mGameButton.setOnClickListener {
            homeButtonSubject.onNext(GAMES_BUTTON_ID)
            switchToGamesFragment()
        }
        mBookingButton.setOnClickListener({
            homeButtonSubject.onNext(CALENDAR_BUTTON_ID)
            switchToBookingFragment()
        })
        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(this) { isOpen ->
            if (isOpen) {
                mHomeButtons.visibility = View.GONE
            } else {
                restoreHomeButtonsVisibility()
            }
        }
        mToolbar.setOnLeftButtonClickListener {
            supportFragmentManager.popBackStack()
            mToolbar.hideLeftButton()
            restoreHomeButtonsVisibility()
        }
        switchToHomeFragment()
    }

    private fun restoreHomeButtonsVisibility() {
        Handler().postDelayed({ mHomeButtons.visibility = View.VISIBLE }, 100)
    }

    private fun switchToHomeFragment() {
        val fragment: HomeTabFragment = HomeTabFragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, HOME_TAG)
        transaction.commit()
    }

    private fun switchToGamesFragment() {
        val fragment: GameTabFragment = GameTabFragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, GAME_TAG)
        transaction.commit()
    }

    private fun switchToBookingFragment() {
        val fragment: BookingFragment = BookingFragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, BOOKING_TAG)
        transaction.commit()
    }


    open fun switchToGameDetailFragment(game : Game) {
        mHomeButtons.visibility = GONE
        mToolbar.showLeftButton()
        val fragment: GameDetailFragment = GameDetailFragment.newInstance(game)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, GAME_DETAIL_TAG)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0 && supportFragmentManager.findFragmentByTag(GAME_DETAIL_TAG) != null) {
            supportFragmentManager.popBackStack()
            mToolbar.hideLeftButton()
            restoreHomeButtonsVisibility()
        } else {
            super.onBackPressed()
        }
    }

    open fun goToLogin() {
        val i = Intent()
        i.setClass(this, LoginActivity::class.java)
        startActivity(i)
        finish()
    }


}