package it.mobile.android.pandorum.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import it.mobile.android.pandorum.feature.home.HomeActivity;

public class HomeButton extends FrameLayout {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Observable<String> observable;
    private Consumer consumer = new Consumer<String>() {
        @Override
        public void accept(String s) throws Exception {
            if (s.equals(HomeActivity.HOME_BUTTON_ID)) {
                setSelected(true);
            }
            else {
                setSelected(false);
            }
        }
    };

    public HomeButton(@NonNull Context context) {
        super(context);
    }

    public HomeButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPublishSubject(Observable<String> observable) {
        unBind();
        this.observable = observable;
        bind();
    }

    public void bind() {
        compositeDisposable.add(observable.subscribe(consumer));
    }

    public void unBind() {
        compositeDisposable.clear();
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
