package it.mobile.android.pandorum.feature.home.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.model.Game

class GamesAdapterFiltered(private var games: List<Game>? = null, val onGameClicked: ((game : Game) -> Unit)) : RecyclerView.Adapter<GamesAdapterFiltered.GamesViewHolder>(), Filterable {

    private var filteredList: List<Game>? = games

    override fun getFilter(): Filter {
        return GameFilter()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GamesViewHolder {
        return GamesViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.game_element_vertical, parent, false), onGameClicked)
    }

    override fun onBindViewHolder(holder: GamesViewHolder?, position: Int) {
        holder!!.onBind(filteredList?.get(position)!!)
    }

    class GamesViewHolder : ViewHolder, View.OnClickListener {

        constructor(itemView: View?, onGameClicked: ((game : Game) -> Unit)) : super(itemView) {
            this.onGameClicked = onGameClicked
            this.imageView = itemView!!.findViewById(R.id.gameImage)
            this.header = itemView!!.findViewById(R.id.textHeader)
            this.subHeader = itemView!!.findViewById(R.id.textMessage)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onGameClicked(game)
        }

        private var imageView: ImageView
        private var header: TextView
        private var subHeader: TextView

        private lateinit var game: Game
        val onGameClicked: ((game : Game) -> Unit)

        fun onBind(game: Game) {
            this.game = game
            header.text = game.name
            subHeader.text = game.descr
            if (game.isScaleToFill != null && game.isScaleToFill!!) {
                imageView.scaleType = ImageView.ScaleType.FIT_XY
            } else {
                imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
            }
            Picasso.get().load(game.imgUrl).into(imageView)
        }

    }

    inner class GameFilter : Filter() {
        override fun performFiltering(charSequence: CharSequence?): FilterResults {
            filteredList = if (charSequence!!.isEmpty()) {
                games
            } else {
                val temp = ArrayList<Game>()
                for (row in games!!) {
                    if (row.descr!!.toLowerCase().contains(charSequence, true) || row.name!!.toLowerCase().contains(charSequence, true)) {
                        temp.add(row)
                    }
                }
                temp
            }
            val filterResults = Filter.FilterResults()
            filterResults.values = filteredList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            filteredList = results!!.values as List<Game>
            notifyDataSetChanged()
        }

    }


    override fun getItemCount(): Int {
        return filteredList?.size ?: 0
    }


}