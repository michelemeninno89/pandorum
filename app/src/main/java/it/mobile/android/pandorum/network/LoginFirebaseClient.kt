package it.mobile.android.pandorum.network

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import it.mobile.android.pandorum.model.Accessory
import it.mobile.android.pandorum.model.Cocktail
import it.mobile.android.pandorum.model.Game
import it.mobile.android.pandorum.utils.CryptoUtils
import it.mobile.android.pandorum.model.User
import it.mobile.android.pandorum.storage.HomeDataCache

class LoginFirebaseClient {


    private val mRootRef = FirebaseDatabase.getInstance().reference
    private val mUserRef = mRootRef.child("users")
    private val mCocktailRef = mRootRef.child("cocktails")
    private val mGamesRef = mRootRef.child("games")
    private val mAccessoriesRef = mRootRef.child("accessories")
    private val mAuth = FirebaseAuth.getInstance()


    constructor()

    fun signIn(username: String, password: String, success: (s: String) -> Unit, failure: (s: String) -> Unit) {
        mUserRef.orderByChild("username").equalTo(username).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                //
            }

            override fun onDataChange(p0: DataSnapshot?) {
                if (p0?.exists()!!) {
                    p0.children
                            .map { i -> i.getValue(User::class.java)!! }
                            .forEach {
                                if (CryptoUtils.decryptString(it.password!!) == password && it.username == username) {
                                    var x:Int =0
                                    downloadCocktails({
                                        x++
                                        if(x==3){
                                            success("")
                                        }
                                    }, {
                                        failure("login fallita")
                                    })
                                    downloadGames({x++
                                        if(x==3){
                                            success("")
                                        }}, {
                                        failure("login fallita")
                                    })
                                    downloadAccessories({x++
                                        if(x==3){
                                            success("")
                                        }}, {
                                        failure("login fallita")
                                    })

                                }
                                else {
                                    failure("password errata")
                                }

                            }

                }
                else{
                    failure("username errata")
                }
            }
        })
    }


    fun signInAnonymously(success: () -> Unit, error: () -> Unit) {
        mAuth.signInAnonymously().addOnCompleteListener { p0 ->
            if (p0.isSuccessful) {
                success()
            } else {
                error()
            }
        }
    }

    fun downloadCocktails(success: () -> Unit, error: () -> Unit) {
        mCocktailRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                //
            }

            override fun onDataChange(p0: DataSnapshot?) {
                var cocktails = mutableListOf<Cocktail>()
                if (p0?.exists()!!) {
                    cocktails.addAll(p0.children.map { i -> i.getValue(Cocktail::class.java)!! })
                    HomeDataCache.cocktails = cocktails
                }
                success()
            }

        })
    }

    fun downloadGames(success: () -> Unit, error: () -> Unit) {
        mGamesRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                //
            }

            override fun onDataChange(p0: DataSnapshot?) {
                var games = mutableListOf<Game>()
                if (p0?.exists()!!) {
                    for ( snap in p0.children){
                        val value :Game? = snap.getValue(Game::class.java)
                        if(snap.hasChild("isScaleToFill")){
                            value?.isScaleToFill = true
                        }
                        games.add(value!!)
                    }
                    HomeDataCache.games = games
                }
                success()
            }
        })
    }

    fun downloadAccessories(success: () -> Unit, error: () -> Unit) {
        mAccessoriesRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                //
            }

            override fun onDataChange(p0: DataSnapshot?) {
                var accessories = mutableListOf<Accessory>()
                if (p0?.exists()!!) {
                    for ( snap in p0.children){
                        val value :Accessory? = snap.getValue(Accessory::class.java)
                        if(snap.hasChild("isScaleToFill")){
                            value?.isScaleToFill = true
                        }
                        accessories.add(value!!)
                    }
                    HomeDataCache.accessories = accessories
                }
                success()
            }
        })
    }


}