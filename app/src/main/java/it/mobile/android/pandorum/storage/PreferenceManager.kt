package it.mobile.android.pandorum.storage

import android.content.Context
import android.content.SharedPreferences

object PreferenceManager {

    const val USERNAME = "USERNAME"
    const val PASSWORD = "PASSWORD"
    const val COCKTAILS = "COCKTAILS"

    @JvmOverloads
    fun getSharedPreferences(context: Context, preferenceKey: String? = null): SharedPreferences {
        return context.getSharedPreferences(preferenceKey, Context.MODE_PRIVATE)
    }

    @JvmOverloads
    @Synchronized
    fun savePreference(key: String, obj: Any, context: Context, preferenceKey: String? = null) {
        if (obj is String) {
            getSharedPreferences(context, preferenceKey).edit().putString(key, obj).apply()
        }
    }


}