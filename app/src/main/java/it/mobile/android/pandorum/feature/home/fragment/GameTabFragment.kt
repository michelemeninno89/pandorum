package it.mobile.android.pandorum.feature.home.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.feature.home.HomeActivity
import it.mobile.android.pandorum.feature.home.adapter.GamesAdapterFiltered
import it.mobile.android.pandorum.feature.home.adapter.SpaceItemDecoration
import it.mobile.android.pandorum.storage.HomeDataCache

class GameTabFragment :Fragment() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mEditText : EditText

    companion object {
        fun newInstance() = GameTabFragment().apply {

        }
    }

    override fun onResume() {
        super.onResume()
        closeKeyboard()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflate = inflater!!.inflate(R.layout.fragment_game_tab, container, false)
        mRecyclerView = inflate.findViewById(R.id.gamesRecyclerView)
        mEditText = inflate.findViewById(R.id.search_edit_text)
        if(HomeDataCache.games!=null) {
            val gamesAdapterFiltered = GamesAdapterFiltered(HomeDataCache.games,{
                (activity as HomeActivity).switchToGameDetailFragment(it)
            })
            mRecyclerView.adapter = gamesAdapterFiltered
            mEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    gamesAdapterFiltered.filter.filter(s)
                }

            })
        }
        mRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerView.addItemDecoration(SpaceItemDecoration(resources.getDimension(R.dimen.default_padding), true, true))
        return inflate
    }

    private fun closeKeyboard(){
        val imm :InputMethodManager  = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }
}