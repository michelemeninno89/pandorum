package it.mobile.android.pandorum.storage

import it.mobile.android.pandorum.model.Accessory
import it.mobile.android.pandorum.model.Cocktail
import it.mobile.android.pandorum.model.Game

object HomeDataCache {

    var cocktails : List<Cocktail>? = null
    var games : List<Game>? = null
    var accessories : List<Accessory>? = null

}