package it.mobile.android.pandorum.feature.home.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.model.Cocktail

class CocktailAdapter(private val cocktails: List<Cocktail>? = null) : RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CocktailViewHolder {
        return CocktailViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.cocktail_element, parent, false))
    }

    override fun onBindViewHolder(holder: CocktailViewHolder?, position: Int) {
        holder!!.onBind(cocktails?.get(position)!!)
    }

    class CocktailViewHolder(itemView: View?) : ViewHolder(itemView) {

        private var imageView: ImageView = itemView!!.findViewById(R.id.cocktail_image)
        private lateinit var cocktail: Cocktail

        fun onBind(cocktail: Cocktail) {
            this.cocktail = cocktail
            Picasso.get().load(cocktail.imgUrl).into(imageView)
        }

    }


    override fun getItemCount(): Int {
        return cocktails!!.size
    }


}