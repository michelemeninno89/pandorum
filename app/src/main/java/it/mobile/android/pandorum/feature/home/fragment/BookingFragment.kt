package it.mobile.android.pandorum.feature.home.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.widget.CustomTextView
import java.text.SimpleDateFormat
import java.util.*


class BookingFragment : Fragment() {

    private val array = ArrayList<CustomTextView>()
    private var isTodaySelected = false
    private var firstDayVr1: CustomTextView? = null
    private var secondDayVr1: CustomTextView? = null
    private var thirdDayVr1: CustomTextView? = null
    private var fourthDayVr1: CustomTextView? = null
    private var fifthDayVr1: CustomTextView? = null
    private var sixthDayVr1: CustomTextView? = null
    private lateinit var firstDayVr2: CustomTextView
    private lateinit var secondDayVr2: CustomTextView
    private lateinit var thirdDayVr2: CustomTextView
    private lateinit var fourthDayVr2: CustomTextView
    private lateinit var fifthDayVr2: CustomTextView
    private lateinit var sixthDayVr2: CustomTextView
    private lateinit var firstDayVr3: CustomTextView
    private lateinit var secondDayVr3: CustomTextView
    private lateinit var thirdDayVr3: CustomTextView
    private lateinit var fourthDayVr3: CustomTextView
    private lateinit var fifthDayVr3: CustomTextView
    private lateinit var sixthDayVr3: CustomTextView

    private fun assignViews(view: View) {
        firstDayVr1 = view.findViewById(R.id.first_day_vr1) as CustomTextView
        secondDayVr1 = view.findViewById(R.id.second_day_vr1) as CustomTextView
        thirdDayVr1 = view.findViewById(R.id.third_day_vr1) as CustomTextView
        fourthDayVr1 = view.findViewById(R.id.fourth_day__vr1) as CustomTextView
        fifthDayVr1 = view.findViewById(R.id.fifth_day__vr1) as CustomTextView
        sixthDayVr1 = view.findViewById(R.id.sixth_day__vr1) as CustomTextView

        firstDayVr2 = view.findViewById(R.id.first_day_vr2) as CustomTextView
        secondDayVr2 = view.findViewById(R.id.second_day_vr2) as CustomTextView
        thirdDayVr2 = view.findViewById(R.id.third_day_vr2) as CustomTextView
        fourthDayVr2 = view.findViewById(R.id.fourth_day_vr2) as CustomTextView
        fifthDayVr2 = view.findViewById(R.id.fifth_day_vr2) as CustomTextView
        sixthDayVr2 = view.findViewById(R.id.sixth_day_vr2) as CustomTextView

        firstDayVr3 = view.findViewById(R.id.first_day_vr3) as CustomTextView
        secondDayVr3 = view.findViewById(R.id.second_day_vr3) as CustomTextView
        thirdDayVr3 = view.findViewById(R.id.third_day_vr3) as CustomTextView
        fourthDayVr3 = view.findViewById(R.id.fourth_day_vr3) as CustomTextView
        fifthDayVr3 = view.findViewById(R.id.fifth_day_vr3) as CustomTextView
        sixthDayVr3 = view.findViewById(R.id.sixth_day_vr3) as CustomTextView

        array.add(firstDayVr2)
        array.add(secondDayVr2)
        array.add(thirdDayVr2)
        array.add(fourthDayVr2)
        array.add(fifthDayVr2)
        array.add(sixthDayVr2)
        array.add(firstDayVr3)
        array.add(secondDayVr3)
        array.add(thirdDayVr3)
        array.add(fourthDayVr3)
        array.add(fifthDayVr3)
        array.add(sixthDayVr3)

        firstDayVr2.setOnClickListener {
            selectTodayBackground(firstDayVr2,!firstDayVr2.isSelected)
            firstDayVr2.isSelected = !firstDayVr2.isSelected
            unselectAll(firstDayVr2)
        }
        thirdDayVr2.setOnClickListener {
            selectTodayBackground(thirdDayVr2,!thirdDayVr2.isSelected)
            thirdDayVr2.isSelected = !thirdDayVr2.isSelected
            unselectAll(thirdDayVr2)
        }
        thirdDayVr2.setOnClickListener {
            selectTodayBackground(thirdDayVr2,!thirdDayVr2.isSelected)
            thirdDayVr2.isSelected = !thirdDayVr2.isSelected
            unselectAll(thirdDayVr2)
        }
        fourthDayVr2.setOnClickListener {
            selectTodayBackground(fourthDayVr2,!fourthDayVr2.isSelected)
            fourthDayVr2.isSelected = !fourthDayVr2.isSelected
            unselectAll(fourthDayVr2)
        }
        fifthDayVr2.setOnClickListener {
            selectTodayBackground(fifthDayVr2,!fifthDayVr2.isSelected)
            fifthDayVr2.isSelected = !fifthDayVr2.isSelected
            unselectAll(fifthDayVr2)
        }
        sixthDayVr2.setOnClickListener {
            selectTodayBackground(sixthDayVr2,!sixthDayVr2.isSelected)
            sixthDayVr2.isSelected = !sixthDayVr2.isSelected
            unselectAll(sixthDayVr2)
        }

        firstDayVr3.setOnClickListener {
            selectTodayBackground(firstDayVr3,!firstDayVr3.isSelected)
            firstDayVr3.isSelected = !firstDayVr3.isSelected
            unselectAll(firstDayVr3)
        }
        secondDayVr3.setOnClickListener {
            selectTodayBackground(secondDayVr3,!secondDayVr3.isSelected)
            secondDayVr3.isSelected = !secondDayVr3.isSelected
            unselectAll(secondDayVr3)

        }
        thirdDayVr3.setOnClickListener {
            selectTodayBackground(thirdDayVr3,!thirdDayVr3.isSelected)
            thirdDayVr3.isSelected = !thirdDayVr3.isSelected
            unselectAll(thirdDayVr3)

        }
        fourthDayVr3.setOnClickListener {
            selectTodayBackground(fourthDayVr3,!fourthDayVr3.isSelected)
            fourthDayVr3.isSelected = !fourthDayVr3.isSelected
            unselectAll(fourthDayVr3)
        }
        fifthDayVr3.setOnClickListener {
            selectTodayBackground(fifthDayVr3,!fifthDayVr3.isSelected)
            fifthDayVr3.isSelected = !fifthDayVr3.isSelected
            unselectAll(fifthDayVr3)

        }
        sixthDayVr3.setOnClickListener {
            selectTodayBackground(sixthDayVr3,!sixthDayVr3.isSelected)
            sixthDayVr3.isSelected = !sixthDayVr3.isSelected
            unselectAll(sixthDayVr3)

        }

    }

    fun unselectAll(w: CustomTextView) {
        val cal = Calendar.getInstance()
        val get = cal.get(Calendar.DAY_OF_MONTH)
        for (customTextView in array) {
            if (customTextView != w) {
                customTextView.isSelected = false
            }
            if(!isTodaySelected && customTextView.text.toString().equals(get.toString())){
                customTextView.setTextColor(ContextCompat.getColor(context, R.color.red))
            }
        }
    }


    fun selectTodayBackground(w: CustomTextView, v :Boolean) {
        val cal = Calendar.getInstance()
        val get = cal.get(Calendar.DAY_OF_MONTH)
        if (w.text.toString().equals(get.toString())) {
            isTodaySelected = true
            if(v) {
                w.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
            else {
                w.setTextColor(ContextCompat.getColor(context, R.color.red))
            }
        }
        else{
            isTodaySelected = false
        }
    }

    companion object {
        const val ARG = "ARG"
        const val DAY_FORMAT = "dd"
        fun newInstance() = BookingFragment().apply {

        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val onCreateView = inflater!!.inflate(R.layout.fragment_booking, container, false)
        return onCreateView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assignViews(view!!)
        assignDates()
        selectToday()
        setPreviousDays()
    }

    private fun assignDates() {
        val cal = Calendar.getInstance()
        cal.firstDayOfWeek = Calendar.MONDAY
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        val dateFormat = SimpleDateFormat(DAY_FORMAT, Locale.ITALY)
        var tue = getNextDay(cal, dateFormat)
        var wed = getNextDay(cal, dateFormat)
        var thu = getNextDay(cal, dateFormat)
        var fri = getNextDay(cal, dateFormat)
        var sat = getNextDay(cal, dateFormat)
        var sun = getNextDay(cal, dateFormat)
        firstDayVr2.text = tue
        secondDayVr2.text = wed
        thirdDayVr2.text = thu
        fourthDayVr2.text = fri
        fifthDayVr2.text = sat
        sixthDayVr2.text = sun
        cal.add(Calendar.DATE, 1)
        tue = getNextDay(cal, dateFormat)
        wed = getNextDay(cal, dateFormat)
        thu = getNextDay(cal, dateFormat)
        fri = getNextDay(cal, dateFormat)
        sat = getNextDay(cal, dateFormat)
        sun = getNextDay(cal, dateFormat)
        firstDayVr3.text = tue
        secondDayVr3.text = wed
        thirdDayVr3.text = thu
        fourthDayVr3.text = fri
        fifthDayVr3.text = sat
        sixthDayVr3.text = sun
    }


    private fun selectToday() {
        val cal = Calendar.getInstance()
        val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
        val isSecondWeek = (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(sixthDayVr2.text.toString()))
        if (!isSecondWeek)
            when (dayOfWeek) {
                3 -> firstDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
                4 -> secondDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
                5 -> thirdDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
                6 -> fourthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
                7 -> fifthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
                1 -> sixthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.red))
            }
        else {
            when (dayOfWeek) {
                3 -> firstDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
                4 -> secondDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
                5 -> thirdDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
                6 -> fourthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
                7 -> fifthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
                1 -> sixthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.red))
            }
        }
    }

    private fun setPreviousDays() {
        val cal = Calendar.getInstance()
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(firstDayVr2.text.toString()))
            firstDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(secondDayVr2.text.toString())) {
            secondDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            secondDayVr2.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(thirdDayVr2.text.toString())) {
            thirdDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            thirdDayVr2.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(fourthDayVr2.text.toString())) {
            fourthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            fourthDayVr2.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(fifthDayVr2.text.toString())) {
            fifthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            fifthDayVr2.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(sixthDayVr2.text.toString())) {
            sixthDayVr2.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            sixthDayVr2.isEnabled = false
        }

        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(firstDayVr3.text.toString())) {
            firstDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            firstDayVr3.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(secondDayVr3.text.toString())) {
            secondDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            secondDayVr3.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(thirdDayVr3.text.toString())) {
            thirdDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            thirdDayVr3.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(fourthDayVr3.text.toString())) {
            fourthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            fourthDayVr3.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(fifthDayVr3.text.toString())) {
            fifthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            fifthDayVr3.isEnabled = false
        }
        if (cal.get(Calendar.DAY_OF_MONTH) > Integer.parseInt(sixthDayVr3.text.toString())) {
            sixthDayVr3.setTextColor(ContextCompat.getColor(context, R.color.warm_grey_50))
            sixthDayVr3.isEnabled = false
        }
    }

    private fun getNextDay(cal: Calendar, dateFormat: SimpleDateFormat): String {
        cal.add(Calendar.DATE, 1)
        val time = cal.time
        return dateFormat.format(time.time)
    }
}