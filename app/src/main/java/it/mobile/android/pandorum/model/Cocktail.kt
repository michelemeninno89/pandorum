package it.mobile.android.pandorum.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
 class Cocktail {

    var id: Int? = null
    var imgUrl: String? = null
    var ingredients: List<String>? = null
    var userLiked : List<String>? =null
    var name: String? = null
    var price: Double? = null
    var vetrina: Boolean? = null

    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result["id"] = id!!
        result["imgUrl"] = imgUrl!!
        result["ingredients"] = ingredients!!
        result["name"] = name!!
        result["price"] = price!!
        result["vetrina"] = vetrina!!
        result["usersLiked"] = userLiked!!
        return result
    }

    constructor(){

    }

    constructor(id: Int?, imgUrl: String?,ingredients: List<String>,  name: String?, price: Double?, userLiked: List<String> , vetrina: Boolean? ) {
        this.id = id
        this.imgUrl = imgUrl
        this.name = name
        this.price = price
        this.vetrina = vetrina
        this.ingredients = ingredients
        this.userLiked = userLiked
    }
}