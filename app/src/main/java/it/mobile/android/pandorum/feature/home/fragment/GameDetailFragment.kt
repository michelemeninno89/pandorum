package it.mobile.android.pandorum.feature.home.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.model.Game
import it.mobile.android.pandorum.utils.GameDetailUtil
import it.mobile.android.pandorum.widget.CustomTextView
import org.jetbrains.anko.find


class GameDetailFragment : Fragment() {

    private lateinit var gameImage: ImageView
    private lateinit var textHeader: CustomTextView
    private lateinit var typeDifficultDescr: CustomTextView
    private lateinit var duration: CustomTextView
    private lateinit var numberOfPlayers: CustomTextView
    private lateinit var game: Game
    private lateinit var description: CustomTextView
    private lateinit var instructions: CustomTextView

    private fun assignViews(view: View) {
        description = view.findViewById(R.id.description)
        instructions = view.findViewById(R.id.instructions)
        gameImage = view.findViewById(R.id.gameImage) as ImageView
        textHeader = view.findViewById(R.id.textHeader) as CustomTextView
        typeDifficultDescr = view.findViewById(R.id.type_difficult_descr) as CustomTextView
        duration = view.findViewById(R.id.duration) as CustomTextView
        numberOfPlayers = view.findViewById(R.id.number_of_players) as CustomTextView
    }


    companion object {
        const val ARG: String = "ARG"
        fun newInstance(game: Game) = GameDetailFragment().apply {
            val bundle = Bundle()
            bundle.putSerializable(ARG, game)
            this.arguments = bundle
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val serializable = arguments.getSerializable(ARG)
        game = serializable as Game
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflate = inflater!!.inflate(R.layout.fragment_game_detail, container, false)
        assignViews(inflate)
        return inflate
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        description.text = game.descr
        val difficulty = game.difficulty?.let { GameDetailUtil.getGameDifficultyById(it) }
        val category = game.category?.let { GameDetailUtil.getGameCategoryById(it) }
        typeDifficultDescr.text = category.plus(" - ").plus(difficulty)
        instructions.text = game.inst
        textHeader.text = game.name
        duration.text = game.minDuration.toString().plus(" - ").plus(game.maxDuration.toString().plus(" ").plus("minuti"))
        numberOfPlayers.text = game.minPlayer.toString().plus(" - ").plus(game.maxPlayer.toString().plus(" ").plus("giocatori"))
        if (game.isScaleToFill != null && game.isScaleToFill!!) {
            gameImage.scaleType = ImageView.ScaleType.FIT_XY
        } else {
            gameImage.scaleType = ImageView.ScaleType.CENTER_INSIDE
        }
        Picasso.get().load(game.imgUrl).into(gameImage)
    }


}