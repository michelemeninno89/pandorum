package it.mobile.android.pandorum.feature.home.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.feature.home.HomeActivity
import it.mobile.android.pandorum.feature.home.adapter.AccessoriesAdapter
import it.mobile.android.pandorum.feature.home.adapter.CocktailAdapter
import it.mobile.android.pandorum.feature.home.adapter.GamesAdapter
import it.mobile.android.pandorum.feature.home.adapter.SpaceItemDecoration
import it.mobile.android.pandorum.model.Game
import it.mobile.android.pandorum.storage.HomeDataCache

class HomeTabFragment : Fragment() {

    private lateinit var mCocktailRecyclerView: RecyclerView
    private lateinit var mGamesRecyclerView: RecyclerView
    private lateinit var mAccessoriesRecyclerView: RecyclerView
    private lateinit var showAllGames : View
    private lateinit var showAllCocktails: View

    companion object {
        fun newInstance() = HomeTabFragment().apply{

        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home_tab, container, false)
        mCocktailRecyclerView = view!!.findViewById(R.id.cocktailRecyclerView)
        mGamesRecyclerView = view.findViewById(R.id.gamesRecyclerView)
        mAccessoriesRecyclerView = view.findViewById(R.id.accessoriesRecyclerView)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAccessoriesRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        mGamesRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        mCocktailRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        mCocktailRecyclerView.addItemDecoration(SpaceItemDecoration(resources.getDimension(R.dimen.default_padding)))
        mGamesRecyclerView.addItemDecoration(SpaceItemDecoration(resources.getDimension(R.dimen.default_padding), true))
        mAccessoriesRecyclerView.addItemDecoration(SpaceItemDecoration(resources.getDimension(R.dimen.default_padding)))
        if(HomeDataCache.cocktails!=null && HomeDataCache.games!=null && HomeDataCache.accessories!=null) {
            mCocktailRecyclerView.adapter = CocktailAdapter(HomeDataCache.cocktails)
            mGamesRecyclerView.adapter = GamesAdapter(HomeDataCache.games, object :GamesAdapter.OnGameClickListener{
                override fun onGameClicked(game: Game) {
                    (activity as HomeActivity).switchToGameDetailFragment(game)
                }

            })
            mAccessoriesRecyclerView.adapter = AccessoriesAdapter(HomeDataCache.accessories)
        }
        else{
            (activity as HomeActivity).goToLogin()
        }
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(mGamesRecyclerView)
    }


}