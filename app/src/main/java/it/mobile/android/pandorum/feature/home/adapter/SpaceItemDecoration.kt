package it.mobile.android.pandorum.feature.home.adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlin.math.roundToInt

class SpaceItemDecoration(private val space: Float, private val addAlsoOnTheRight: Boolean? = null, private val addAlsoOnBottomAndTop: Boolean? = null, private val addOnlyOnBottom: Boolean? = null) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        if (addAlsoOnTheRight != null) {
            outRect!!.right = space.roundToInt()
        }
        if (addAlsoOnBottomAndTop != null) {
            outRect!!.top = space.roundToInt()
            if (parent!!.adapter.itemCount-1 == parent.getChildAdapterPosition(view))
                outRect.bottom = space.roundToInt()
        }
        if (addOnlyOnBottom != null) {
            if (parent!!.adapter.itemCount == parent.getChildAdapterPosition(view))
                if (outRect != null) {
                    outRect.bottom = space.roundToInt()
                }
        }
        outRect?.left = space.roundToInt()
    }
}