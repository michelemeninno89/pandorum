package it.mobile.android.pandorum.feature.login

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.Guideline
import android.support.design.widget.TextInputLayout
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.FirebaseApp
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.feature.home.HomeActivity
import it.mobile.android.pandorum.network.LoginFirebaseClient
import it.mobile.android.pandorum.widget.ResizableLoginButton
import org.jetbrains.anko.alert
import it.mobile.android.pandorum.storage.PreferenceManager
import it.mobile.android.pandorum.widget.CustomTextView

class LoginActivity : AppCompatActivity() {

    private lateinit var loginButton: ResizableLoginButton
    private lateinit var logoButton: ResizableLoginButton
    private lateinit var layout: ConstraintLayout
    private lateinit var username: TextInputLayout
    private lateinit var password: TextInputLayout
    private lateinit var loginButtonValue: TextView
    private lateinit var passwordEditTextView: EditText
    private lateinit var usernameEditText: EditText
    private lateinit var guideline: Guideline
    private lateinit var howToRegister :CustomTextView

    private var client: LoginFirebaseClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginButton = findViewById(R.id.resizibleLoginButton)
        logoButton = findViewById(R.id.pandorumLogo)
        layout = findViewById(R.id.layout)
        username = findViewById(R.id.username_layout)
        password = findViewById(R.id.username_password)
        loginButtonValue = findViewById(R.id.login_button_value)
        passwordEditTextView = findViewById(R.id.password_edit_text)
        usernameEditText = findViewById(R.id.username_edit_text)
        guideline = findViewById(R.id.guideline)
        FirebaseApp.initializeApp(this)
        client = LoginFirebaseClient()
        client!!.signInAnonymously(
                success = {
                    if(PreferenceManager.getSharedPreferences(this).contains(PreferenceManager.USERNAME)) {
                        usernameEditText.setText(PreferenceManager.getSharedPreferences(this).getString(PreferenceManager.USERNAME, ""))
                        passwordEditTextView.setText(PreferenceManager.getSharedPreferences(this).getString(PreferenceManager.PASSWORD, ""))
                        loginButton.performClick()
                    }
                },
                error = {
                    //capire cosa fare
                })
    }

    private fun closeLogginPanel() {
        logoButton.fromCenterToTop().start()
        val fromCenterToTop = loginButton.fromCenterToTop()
        fromCenterToTop.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                loginButtonValue.text = getString(R.string.login)
            }
        })
        fromCenterToTop.start()
    }

    private fun openLoggingPanel() {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val zoomToCenterFromTop = loginButton.zoomToCenterFromTop(metrics.heightPixels / 2.0f)
        zoomToCenterFromTop.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                loginButtonValue.text = getString(R.string.loggin)
                performLogin()
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }
        })
        zoomToCenterFromTop.start()
        logoButton.zoomToCenterFromTop(metrics.heightPixels / 2.0f).start()
    }

    fun performLogin() {
        if (isOnline()) {
            client!!.signIn(usernameEditText.text.toString(), passwordEditTextView.text.toString(),
                    success = {
                        PreferenceManager.savePreference(PreferenceManager.PASSWORD, passwordEditTextView.text.toString(), this)
                        PreferenceManager.savePreference(PreferenceManager.USERNAME, usernameEditText.text.toString(), this)
                        finishAffinity()
                        switchToHome()
                    },
                    failure = {
                        showErrorAlert(it)
                        closeLogginPanel()
                    })
        } else {
            showErrorAlert("Connessione internet assente")
            closeLogginPanel()
        }

    }

    override fun onStart() {
        super.onStart()
        loginButton.setOnClickListener({
            if (validateForm()) {
                openLoggingPanel()
            } else {
                showErrorAlert("Inserire delle credenziali valide")
            }
        })

    }

    private fun validateForm(): Boolean {
        if (!usernameEditText.text.isEmpty() && !passwordEditTextView.text.isEmpty()) {
            if (usernameEditText.text.toString().contains("([^A-Za-z0-9!%\$&£()=?àéìòù§*#<>€ç+-_^.])".toRegex())) {
                return false
            }
            if (passwordEditTextView.text.toString().contains("([^A-Za-z0-9])".toRegex())) {
                return false
            }
        } else {
            return false
        }
        return true
    }

    private fun showErrorAlert(string: String) {
        alert(string) {
            setTheme(R.style.Theme_AppCompat_DayNight_Dialog_Alert)
            title = "ERRORE"
            positiveButton("CHIUDI", {

            })
        }.show()
    }

    private fun switchToHome() {
        val intent = Intent()
        intent.setClass(this, HomeActivity::class.java)
        startActivity(intent)
    }


    private fun isOnline(): Boolean {
        val systemService = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return systemService.activeNetworkInfo != null && systemService.activeNetworkInfo.isConnectedOrConnecting
    }

}

