package it.mobile.android.pandorum.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@IgnoreExtraProperties
class Game : Serializable{

    var category: Int? = null
        get() = field
    var descr: String? = null
    var difficulty: Int?= null
    var id: Int?= null
    var imgUrl: String?= null
    var inst: String?= null
    var isScaleToFill: Boolean?= null
        get() = field
    var maxDuration: Int?= null
    var maxPlayer: Int?= null
    var minDuration: Int?= null
    var minPlayer: Int?= null
    var vetrina: Boolean?= null
    var name: String?= null

    constructor()


    constructor(category: Int? , desc: String?, difficulty: Int? , id: Int?, imgUrl: String?, inst: String?, isScaleToFill: Boolean?, maxDuration: Int? , maxPlayer: Int?, minDuration: Int?, minPlayer: Int?, vetrina: Boolean?, name: String? ) {
        this.category = category
        this.descr = desc
        this.difficulty = difficulty
        this.id = id
        this.imgUrl = imgUrl
        this.inst = inst
        this.isScaleToFill = isScaleToFill
        this.maxDuration = maxDuration
        this.maxPlayer = maxPlayer
        this.minDuration = minDuration
        this.minPlayer = minPlayer
        this.vetrina = vetrina
        this.name = name
    }
    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result["category"] = category!!
        result["descr"] = descr!!
        result["difficulty"] = difficulty!!
        result["id"] = id!!
        result["imgUrl"] = imgUrl!!
        result["inst"] = inst!!
        result["isScaleToFill"] = isScaleToFill!!
        result["maxDuration"] = maxDuration!!
        result["maxPlayer"] = maxPlayer!!
        result["minDuration"] = minDuration!!
        result["minPlayer"] = minPlayer!!
        result["name"] = name!!
        result["vetrina"] = vetrina!!
        return result

    }
}