package it.mobile.android.pandorum.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Ingredient {

    private var ingredient: String? = null

    @Exclude
    private fun toMap(): HashMap<String, Any> {
        val result = HashMap<String, Any>()
        return result
    }

    constructor() {

    }

    constructor(ingredient: String?) {
        this.ingredient = ingredient
    }
}