package it.mobile.android.pandorum.utils

import android.content.Context
import android.graphics.Typeface

class FontManager {

    private constructor(context: Context) {
        this.bold = Typeface.createFromAsset(context.assets, FONT_BOLD)
        this.regular = Typeface.createFromAsset(context.assets, FONT_REGULAR)
        this.medium = Typeface.createFromAsset(context.assets, FONT_MEDIUM)
        this.extrabold = Typeface.createFromAsset(context.assets, FONT_EXTRA_BOLD)
    }

    companion object {

        const val FONT_BOLD = "fonts/Raleway-Bold.ttf"
        const val FONT_REGULAR = "fonts/Raleway-Regular.ttf"
        const val FONT_MEDIUM = "fonts/Raleway-Medium.ttf"
        const val FONT_EXTRA_BOLD = "fonts/Raleway-ExtraBold.ttf"

        private var instance: FontManager? = null

        fun getInstance(context: Context): FontManager {
            if (instance == null) {
                instance = FontManager(context)
            }
            return instance!!
        }
    }

    val bold: Typeface
    val regular: Typeface
    val medium: Typeface
    val extrabold: Typeface


}