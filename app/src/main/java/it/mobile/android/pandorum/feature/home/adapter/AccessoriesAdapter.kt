package it.mobile.android.pandorum.feature.home.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.model.Accessory

class AccessoriesAdapter(private val accessories: List<Accessory>? = null) : RecyclerView.Adapter<AccessoriesAdapter.Accessories>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Accessories {
        return Accessories(LayoutInflater.from(parent!!.context).inflate(R.layout.accessories_element, parent, false))
    }

    override fun onBindViewHolder(holder: Accessories?, position: Int) {
        holder!!.onBind(accessories?.get(position)!!)
    }

    class Accessories(itemView: View?) : ViewHolder(itemView) {

        private var imageView: ImageView = itemView!!.findViewById(R.id.accessories_image)
        private lateinit var accessory: Accessory

        fun onBind(accessory: Accessory) {
            this.accessory = accessory
            Picasso.get().load(accessory.imgUrl).into(imageView)
        }

    }

    override fun getItemCount(): Int {
        return accessories!!.size
    }


}