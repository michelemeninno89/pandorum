package it.mobile.android.pandorum.utils

import android.util.Base64

class CryptoUtils {

    companion object {
        private const val key: String = "_VDPandorumApp2018!byRD"

        fun encryptStringBase64(string: String): String {
            val finaString: String = string.plus(key)
            return Base64.encodeToString(finaString.toByteArray(), Base64.URL_SAFE)
        }

        fun decryptString(string: String) : String? {
            val decodeByteArray : ByteArray? = Base64.decode(string.toByteArray(), Base64.URL_SAFE)
            val decodeString :String = decodeByteArray?.let { String(it) }!!
            return if(decodeString.endsWith(key)) {
                decodeString.replaceFirst(key, "")
            } else{
                null
            }
        }
    }
}

