package it.mobile.android.pandorum.feature.home.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.model.Game

class GamesAdapter(private val games: List<Game>? = null, open val onGameClickListener: OnGameClickListener) : RecyclerView.Adapter<GamesAdapter.GamesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GamesViewHolder {
        return GamesViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.game_element, parent, false), onGameClickListener)
    }

    override fun onBindViewHolder(holder: GamesViewHolder?, position: Int) {
        holder!!.onBind(games?.get(position)!!)
    }

    class GamesViewHolder : ViewHolder, View.OnClickListener {

        constructor(itemView: View?, onGameClickListener: OnGameClickListener) : super(itemView) {
            this.imageView = itemView!!.findViewById(R.id.gameImage)
            this.header = itemView!!.findViewById(R.id.textHeader)
            this.subHeader = itemView!!.findViewById(R.id.textMessage)
            itemView.setOnClickListener(this)
            this.onGameClickListener = onGameClickListener
        }

        override fun onClick(v: View?) {
            onGameClickListener.onGameClicked(game)
        }

        private val onGameClickListener : OnGameClickListener
        private var imageView: ImageView
        private var header: TextView
        private var subHeader: TextView

        private lateinit var game: Game

        fun onBind(game: Game) {
            this.game = game
            header.text = game.name
            subHeader.text = game.descr
            if (game.isScaleToFill != null && game.isScaleToFill!!) {
                imageView.scaleType = ImageView.ScaleType.FIT_XY
            } else {
                imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
            }
            Picasso.get().load(game.imgUrl).into(imageView)
        }

    }


    override fun getItemCount(): Int {
        return games?.size ?: 0
    }

    open interface OnGameClickListener {
        fun onGameClicked(game: Game)
    }


}