package it.mobile.android.pandorum.utils;

public class GameDetailUtil {

    public static String getGameDifficultyById(int id){
        switch (id){
            case 0:
                return "Facile";
            case 1:
                return "Medio";
            case 2:
                return "Difficile";
            default:
                return "";
        }
    }

    public static String getGameCategoryById(int id){
        switch (id){
            case 0:
                return "Strategia";
            case 1:
                return "Party Game";
            case 2:
                return "Narrativa";
            case 3:
                return "Duello";
            case 4:
                return "Didattiva";
            default:
                return "";
        }
    }


}
