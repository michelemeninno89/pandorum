package it.mobile.android.pandorum.widget

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import it.mobile.android.pandorum.R

class ResizableLoginButton : CardView {

    constructor(context: Context) : this(context, null) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    fun zoomToCenterFromTop(screenHeight: Float): Animator {
        val myHeight = resources.getDimensionPixelSize(R.dimen.login_button_height)
        val offeset = resources.getDimensionPixelSize(R.dimen.offest_login)

        val y = screenHeight - offeset
        val animator: ValueAnimator = ValueAnimator.ofFloat(myHeight * 1.0f, y)
        animator.addUpdateListener({
            val animatedValue = it.animatedValue as Float
            layoutParams.height = animatedValue.toInt()
            requestLayout()

        })
        animator.interpolator = OvershootInterpolator(0.9f)
        animator.duration = 1000L

        return animator
    }

    fun fromCenterToTop(): Animator {
        val myHeight = resources.getDimensionPixelSize(R.dimen.login_button_height)
        val animator: ValueAnimator = ValueAnimator.ofInt(measuredHeight, myHeight)
        animator.addUpdateListener({
            val animatedValue = it.animatedValue as Int
            layoutParams.height = animatedValue
            requestLayout()

        })
        animator.interpolator = LinearInterpolator()
        animator.duration = 1000
        return animator
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }


    fun init() {
    }
}