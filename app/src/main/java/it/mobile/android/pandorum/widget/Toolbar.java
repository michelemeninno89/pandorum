package it.mobile.android.pandorum.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import it.mobile.android.pandorum.R;

public class Toolbar extends FrameLayout {

    private View leftButton;
    private OnLeftButtonClickListener onLeftButtonClickListener;


    public Toolbar(@NonNull Context context) {
        super(context);
    }

    public Toolbar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Toolbar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        leftButton = findViewById(R.id.left_button);
        leftButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onLeftButtonClickListener != null) {
                    onLeftButtonClickListener.onLeftButtonClick();
                }
            }
        });
    }

    public void setOnLeftButtonClickListener(OnLeftButtonClickListener onLeftButtonClickListener) {
        this.onLeftButtonClickListener = onLeftButtonClickListener;
    }

    interface OnLeftButtonClickListener {
        void onLeftButtonClick();
    }

    public void showLeftButton() {
        leftButton.setVisibility(VISIBLE);

    }

    public void hideLeftButton() {
        leftButton.setVisibility(INVISIBLE);
    }
}
