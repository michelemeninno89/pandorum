package it.mobile.android.pandorum.widget;

import android.animation.TimeInterpolator;
import android.view.animation.LinearInterpolator;

public class CustomInterpolator extends LinearInterpolator implements TimeInterpolator{

    @Override
    public float getInterpolation(float input) {
        return (float) Math.sin(input*input);
    }
}
