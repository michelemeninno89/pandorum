package it.mobile.android.pandorum.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class User {
    var cardNumber : Int? = null
    var isAdmin : Boolean? = null
    var password :String? = null
    var pe: Int? = null
    var point :Int? = null
    var username :String? = null

    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result.put("cardNumber", cardNumber!!)
        result.put("isAdmin", isAdmin!!)
        result.put("password", password!!)
        result.put("pe", pe!!)
        result.put("point", point!!)
        result.put("username", username!!)
        return result
    }

    constructor(){
        //
    }

    constructor(cardNumber: Int?, isAdmin: Boolean?, password: String, pe: Int?, point: Int?, username: String?) {
        this.cardNumber = cardNumber
        this.isAdmin = isAdmin
        this.password = password
        this.pe = pe
        this.point = point
        this.username = username
    }

}