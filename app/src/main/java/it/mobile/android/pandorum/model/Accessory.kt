package it.mobile.android.pandorum.model

import com.google.firebase.database.Exclude

class Accessory {

    var descr: String? = null
    var id: Int?= null
    var imgUrl: String?= null
    var inst: String?= null
    var isScaleToFill: Boolean?= null
        get() = field
    var vetrina: Boolean?= null
    var price: Double?= null

    constructor()


    constructor(category: Int? , desc: String?, id: Int?, imgUrl: String?, inst: String?, isScaleToFill: Boolean? , vetrina: Boolean?, price: Double? ) {
        this.descr = desc
        this.id = id
        this.imgUrl = imgUrl
        this.inst = inst
        this.isScaleToFill = isScaleToFill
        this.vetrina = vetrina
        this.price = price
    }
    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result["descr"] = descr!!
        result["id"] = id!!
        result["imgUrl"] = imgUrl!!
        result["inst"] = inst!!
        result["isScaleToFill"] = isScaleToFill!!
        result["vetrina"] = vetrina!!
        result["price"] = price!!
        return result

    }
}