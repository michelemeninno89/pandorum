package it.mobile.android.pandorum.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import it.mobile.android.pandorum.R;
import it.mobile.android.pandorum.utils.FontManager;

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        int font[] = {R.attr.font_name};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, font);
        String fontName = typedArray.getString(R.styleable.custom_font_name);
        setFont(fontName != null ? fontName : FontManager.FONT_REGULAR);
        typedArray.recycle();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int font[] = {R.attr.font_name};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, font);
        String fontName = typedArray.getString(R.styleable.custom_font_name);
        setFont(fontName != null ? fontName : FontManager.FONT_REGULAR);
        typedArray.recycle();
    }

    private void setFont(String fontName) {
        switch (fontName) {
            case FontManager.FONT_REGULAR:
                setTypeface(FontManager.Companion.getInstance(this.getContext()).getRegular());
                break;
            case FontManager.FONT_MEDIUM:
                setTypeface(FontManager.Companion.getInstance(this.getContext()).getMedium());
                break;
            case FontManager.FONT_BOLD:
                setTypeface(FontManager.Companion.getInstance(this.getContext()).getBold());
                break;
            case FontManager.FONT_EXTRA_BOLD:
                setTypeface(FontManager.Companion.getInstance(this.getContext()).getBold());
                break;
        }


    }

}
