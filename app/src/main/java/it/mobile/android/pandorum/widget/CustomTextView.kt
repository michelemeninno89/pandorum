package it.mobile.android.pandorum.widget

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.TextView
import it.mobile.android.pandorum.R
import it.mobile.android.pandorum.utils.FontManager

class CustomTextView : TextView {

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {
        val font: IntArray = intArrayOf(R.attr.font_name)
        val tA: TypedArray = context.obtainStyledAttributes(attrs, font)
        val fontName: String = tA.getString(R.styleable.custom_font_name)
        tA.recycle()
        setFont(fontName)
    }

    private fun setFont(fontName: String? = FontManager.FONT_REGULAR) {
        when (fontName) {
            FontManager.FONT_REGULAR -> typeface = FontManager.getInstance(context = this.context).regular
            FontManager.FONT_BOLD -> typeface = FontManager.getInstance(context = this.context).bold
            FontManager.FONT_MEDIUM -> typeface = FontManager.getInstance(context = this.context).medium
            FontManager.FONT_EXTRA_BOLD -> typeface = FontManager.getInstance(context = this.context).extrabold
        }
    }

}